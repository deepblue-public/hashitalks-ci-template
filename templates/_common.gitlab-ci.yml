---

variables:
  CI_REGISTRY: registry.gitlab.com
  CI_REGISTRY_USER: gitlab-ci-token
  CI_IMAGE_NAME: ${CI_REGISTRY}/<my image>
  CI_IMAGE_TAG: latest
  CI_IMAGE: ${CI_IMAGE_NAME}:${CI_IMAGE_TAG}
  OCI_REGISTRY: <my registry> 
  ARTIFACT_DIR: "./archive"
  ARTIFACT_NAME: "built-with-default-${CI_PROJECT_NAME}.tar"
  ARTIFACT_TAG: ${CI_COMMIT_REF_SLUG} # OCI_IMAGE_TAG
  APP_IMAGE: $APP
  OCI_IMAGE_TAG_MR_PREFIX: uat
  BUILDAH_SCRIPT: "build-oci.sh"
  CI_COMMIT_TAG_MR_SUFFIX: ""
  NOMAD_IMAGE: ${CI_REGISTRY}/<my image>
  NOMAD_ADDR_DEV: <my url>
  NOMAD_ADDR_UAT: <my url>
  NOMAD_ADDR_PROD: <my url>
  CODE_QUALITY_IMAGE: "registry.gitlab.com/gitlab-org/ci-cd/codequality:0.87.0"  # Upgrade 0.85.29 for eslint es2021

default:
  image: $CI_IMAGE

# See https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/Branch-Pipelines.gitlab-ci.yml
# - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
# Instead I use base_rules and release_rules

# base_rules allow to run pipeline only on main branch. Main branch should always run the full pipeline (maybe without quality and security)
.base_rules: &base_rules
  rules:
    - if: $CI_COMMIT_BRANCH == 'main'

# release_rules allow to run pipeline only on tags
.release_rules: &release_rules
  rules:
    - if: $CI_COMMIT_TAG =~ /^(prod|uat|dev)-/

# See https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml
# - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
# mr_rules allow to run pipeline only on open merge request
# mr_rules must be the last rules
.mr_rules: &mr_rules
  rules:
    - if: $CI_MERGE_REQUEST_ID   # Ensure that the scanning is only executed on merge requests
      when: always
    - when: never

# default_rules include mr_rules, but is targeting dev usage
# default_rules is using mr_rules, then it must be the last rules
.default_rules: &default_rules
  rules:
    - if: $CI_MERGE_REQUEST_TITLE =~ /\b(?i)(draft|wip)\b/
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ /^.*(#uat|#dev).*$/'
    - !reference [.mr_rules, rules]

.dev-build-rules: &dev-build-rules
  rules:
    - if: $CI_COMMIT_TAG =~ /^dev-/
    - if: '$CI_COMMIT_MESSAGE =~ /^.*#dev.*$/s'

.uat-build-rules: &uat-build-rules
  rules:
    - if: $CI_COMMIT_TAG =~ /^uat-/
    - if: '$CI_COMMIT_MESSAGE =~ /^.*#uat.*$/s'

.prod-build-rules: &prod-build-rules
  rules:
    - if: $CI_COMMIT_TAG =~ /^prod-/

# Feel free to override me if you want to login to gitlab by default instead of ecr
# By default image are pushed to ecr (only build image are pushed to gitlab manually)
.login-registry: &login-registry
  script:
    - echo "Login to registry default ${OCI_REGISTRY}/${APP_IMAGE}"
    - !reference [.login-ecr, script]
    #- !reference [.login-gitlab, script]

.login-registry-docker: &login-registry-docker
  script:
    - echo $DOCKER_PASSWORD | docker login --username $DOCKER_USERNAME --password-stdin ${OCI_REGISTRY%/*} || true

.login-registry-buildah: &login-registry-buildah
  script:
    - echo $DOCKER_PASSWORD | buildah login --username $DOCKER_USERNAME --password-stdin ${OCI_REGISTRY%/*} || true

.login-registry-podman: &login-registry-podman
  script:
    - echo $DOCKER_PASSWORD | podman login --username $DOCKER_USERNAME --password-stdin ${OCI_REGISTRY%/*} || true

# Override me in buildah or docker
.login-ecr: &login-ecr
  script:
    - echo "Login to ecr default ${OCI_REGISTRY}/${APP_IMAGE}"
    - aws --version
    - !reference [.login-registry-docker, script]

.login-gitlab-docker: &login-gitlab-docker
  script:
    - docker login -u "$CI_REGISTRY_USER" -p $CI_JOB_TOKEN $CI_REGISTRY || true

.login-gitlab-buildah: &login-gitlab-buildah
  script:
    - buildah login -u "$CI_REGISTRY_USER" -p $CI_JOB_TOKEN $CI_REGISTRY || true

.login-gitlab-podman: &login-gitlab-podman
  script:
    - podman login -u "$CI_REGISTRY_USER" -p $CI_JOB_TOKEN $CI_REGISTRY || true

# Override me in buildah or docker
.login-gitlab: &login-gitlab
  script:
    - echo "Login to gitlab default ${OCI_REGISTRY}/${APP_IMAGE}"
    - !reference [.login-gitlab-docker, script]

# Same as docker-image (-> DOCKER_IMAGE) and buildah-image (-> BUILDAH_IMAGE) in buildah-build and oci-build
.oci-image: &oci-image
  script:
    - |
      if [ -z $CI_COMMIT_TAG ]; then
        OCI_IMAGE_TAG="$OCI_IMAGE_TAG_MR_PREFIX$CI_COMMIT_TAG_MR_SUFFIX-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
      else
        OCI_IMAGE_TAG="$CI_COMMIT_TAG$CI_COMMIT_TAG_MR_SUFFIX-$CI_COMMIT_SHORT_SHA"
      fi
    - echo "OCI_IMAGE=$APP_IMAGE:$OCI_IMAGE_TAG" >> build.env && export OCI_IMAGE_TAG=$OCI_IMAGE_TAG && export OCI_IMAGE=$APP_IMAGE:$OCI_IMAGE_TAG
    - echo "OCI_IMAGE=$OCI_IMAGE"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      exists:
        - 'Dockerfile'
        - 'Containerfile'
        - '$BUILDAH_SCRIPT'

.code_quality: &code_quality
  stage: build
  #variables:
  #  CODE_QUALITY_IMAGE: "registry.gitlab.com/gitlab-org/ci-cd/codequality:0.85.29"  # Upgrade 0.85.29 for eslint es2021
  artifacts:
    paths: [gl-container-scanning-report.json, gl-codeclimate-image.json, gl-codeclimate-fs.json, gl-codeclimate.json]
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - !reference [.dev-build-rules, rules]
    - !reference [.uat-build-rules, rules]
    - !reference [.default_rules, rules]
